CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------
Inserts the contents of a block into into a node using [block:module=delta] tags
or the wysiwyg editor by enabling the editors plugin.

Currently supported editors:

* TinyMCE


REQUIREMENTS
------------
This module requires the following modules:
 * Bean (https://www.drupal.org/project/bean)
 * Wysiwyg (https://www.drupal.org/project/wysiwyg)


INSTALLATION
------------
Install and enable as you would normally install a contributed Drupal module.


CONFIGURATION
-------------
After enabling the module the Block Wysiwyg filter needs to be added to the
relevant content filter. This can be done from the configure button on the
module listing page.

To enable the editor plugin, configure the Wysiwyg profile and under plugins
select Block Wysiwyg. This will add the relevant button on the tool bar.

Any block can be added into the editor (views, webform, beans) however using
the editor gui it is limited to beans.

If you would like the enable embedding dynamic blocks (such as views) then
caching needs to be disabled and the shortcodes used like so:

[block:view=my-custom-view]

Where view is the module and my-custom-view is the delta.

This information can be found form the block configuration screen:

admin/structure/block/manage/views/my-custom-view/configure


MAINTAINERS
-----------
Current maintainers:
 * Jake Paine (painejake) - https://www.drupal.org/user/2866943
 * Joel Evans (joele75) - https://www.drupal.org/user/2477322
