/**
 * @file
 * Block Wysiwyg - Druapl Module.
 *
 * The TinyMCE plugin.
 */

jQuery(document).ready(function($) {

  tinymce.create('tinymce.plugins.block_wysiwyg', {
    /**
     * Initializes the plugin, this will be executed after the plugin has been created.
     * This call is done before the editor instance has finished it's initialization so use the onInit event
     * of the editor instance to intercept that event.
     *
     * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
     * @param {string} url Absolute URL to where the plugin is located.
     */
    init : function(ed, url) {

      ed.addButton('block_wysiwyg', {
        title : 'Block Wysiwyg',
        image : 'http://dummyimage.com/200x200/c2c2c2/fff.png&text=B',
        onclick : function(event) {
          tinyMCE.activeEditor.windowManager.open({
            file : '?q=admin/block_wysiwyg',
            width : 920,
            height : 560,
            inline : true,
            popup_css : false,
          });
        }
      });
    },
  });

  tinymce.PluginManager.add('block_wysiwyg', tinymce.plugins.block_wysiwyg);
});
