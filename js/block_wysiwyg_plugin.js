/**
 * @file
 * Block Wysiwyg - Druapl Module.
 *
 * The TinyMCE plugin functionality.
 */

jQuery(document).ready(function($) {

  var ed = tinyMCEPopup.editor

  $('.col-insert a').click(function() {

    var module = $(this).parent('.col-insert').siblings('.col-module').html();
    var delta = $(this).parent('.col-insert').siblings('.col-delta').html();

    ed.selection.setContent('[block:' + module + '=' + delta + ']');

    tinyMCEPopup.close();
  });
});
